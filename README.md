## PROJET BUDGET APP

# Conception

Pour ce projet j'ai commencé par créer 4 principales classes : 
- Income (entrées)
- Expenses (dépenses)
- Category
- Statistics
  

Ensuite j'ai créer les classes optionnelles (non détaillées) : 
- Remainder (solde)
- RecurrentIncome (opération récurrente)
- Bills (justificatifs)


La classe Income contiendra toutes les entrées du budget et la classe Expenses les dépenses.
La classe Category permettra de classer les opérations par catégorie puis la classe Statistics pourra générer des statistiques
mensuelles de dépenses en % par catégories.

    
La classe Remainder permettra de consulter le solde, la classe RecurrentIncome servira à répéter une opération tous les mois.  Enfin, la classe Bills permettra d'ajouter des justificatifs de dépenses.    
